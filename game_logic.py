from pathlib import Path
from background import *


#Produkty z pierwszej alejki
produkty_pierwsza_alejka = [
    "jajka", "masło", "jogurt", "ser", "śmietana", "świeże warzywa", 
    "owoce","makaron", "ryż", "mąka", "cukier", "sól", "przyprawy", 
    "olej","płatki śniadaniowe", "herbatniki"
]

#Produkty z drugiej alejki
produkty_druga_alejka = [
    "piwo", "wino", "whiskey", "rum", "bułki", "chleb", "ciastka", 
    "pączki","płyn do naczyń", "pasta do zębów", "krem", 
    "papier toaletowy","worki na śmieci"
]


#Stworzenie pomieszczeń, z nazwa,scieżka opisu, i przedmiotami 
intro = Path.cwd() / "przedstawienie.txt"
alejka_pierwsza = Pomieszczenie("Pierwsza Alejka", Path.cwd() / "alejka pierwsza.txt", produkty_pierwsza_alejka)
alejka_druga = Pomieszczenie("Druga Alejka", Path.cwd() / "alejka druga.txt", produkty_druga_alejka)
kasa = Pomieszczenie("Kasa", Path.cwd() / "kasa sklepowa.txt", None)


#stworzenie gry i aktualnej alejki
aktualna_alejka = None
gra = Gra()


#intro gry
def game_intro():
    with intro.open(mode="r", encoding="utf-8", newline="")as file:
        for row in file:
            print(row)
        print("=============" * 10)


#sprawdza komendy używane prez gracza
def command_checker(polecenie_gracza):
    if polecenie_gracza == "opisz":
        print("")
        Pomieszczenie.sprawdz_opis(aktualna_alejka)
    
    elif polecenie_gracza == "pomoc":
        gra.pomoc()

    elif polecenie_gracza == "sprawdź liste":
        gra.sprawdz_liste_zakupow()
    
    elif polecenie_gracza == "weź":
        print("")
        if aktualna_alejka == alejka_pierwsza:
            gra.wez(alejka_pierwsza.przedmioty)  # tu trzeba bedzie poprawić
        elif aktualna_alejka == alejka_druga:
            gra.wez(alejka_druga.przedmioty)
        else:
            print("Nie ma tutaj nic ciekawego do zebrania")
    
    elif polecenie_gracza == "użyj":
        print("")
        gra.uzyj()
    
    elif polecenie_gracza == "sprawdź koszyk":
        gra.sprawdz_koszyk()

    elif polecenie_gracza == "idź dalej":
        pass
    
    elif polecenie_gracza == "wyjdź":
        print("")
        gra.wyjdz()
    else:
        print("niepoprawna komenda, wpisz('pomoc')")


#Wyglad i logika gry
def game():
    global aktualna_alejka
    print("=============" * 10)
    print("wpisz ('pomoc') w celu sprawdzenia możliwych komend.")
    print("=============" * 10)
    while True: #Pętla Gry
        game_intro()

        print("Właśnie trafiłeś do pierwszej alejki sklepowej")
        
        while True:  # Pętla pierwszej alejki
            aktualna_alejka = alejka_pierwsza

            polecenie_gracza = input("co chcesz zrobić?:  ")
            command_checker(polecenie_gracza)
            if polecenie_gracza == "idź dalej":
                print("=============" * 10)
                print("Wchodzisz do drugiej alejki sklepowej")
                break
        
        while True:  # Pętla drugiej alejki
            aktualna_alejka = alejka_druga
            polecenie_gracza = input("co chcesz zrobić?: ")
            command_checker(polecenie_gracza)
            if polecenie_gracza == "idź dalej":
                print("=============" * 10)
                print("Właśnie stoisz przy kasie sklepowej")
                break
        while True:  # Pętla kasy
            aktualna_alejka = kasa
            polecenie_gracza = input("co chcesz zrobić?: ")
            command_checker(polecenie_gracza) 
            if polecenie_gracza.lower() == "użyj":
                print("czym chcesz zapłacić")
                przedmiot_użycia = gra.uzyj()

                if przedmiot_użycia == "portfel" or "telefon":
                    print("-------------" * 10)
                    print("Zapłaciłeś za zakupy")
                    print("Sprawdzasz czy wszystko kupiłeś")
                    print("-------------" * 10)
                    if sorted(gra.lista_zakupow) == sorted(gra.koszyk):
                        print("")
                        print("!!GRATULACJE!!")
                        print("Kupiłeś wszystko z listy!")
                        print("=============" * 10)
                        break
                    else:
                        print("")
                        print("NIE KUPIŁEŚ WSZYSTKIEGO!!")
                        print("W domu będzie AFERA!!")
                        print("=============" * 10)
                        break

                else:
                    print("nie możesz tego tutaj użyć")
            else:
                polecenie_gracza         
    
        break
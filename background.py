
#Klasa Pomieszczenia
class Pomieszczenie:
    def __init__(self, nazwa, sciezka_opisu, przedmioty):
        self.nazwa = nazwa
        self.opis = sciezka_opisu
        self.przedmioty = przedmioty
        self.opis_wyswietlony = False


    def sprawdz_opis(self):
        try:
            print("-------------" * 10)
            with open(self.opis, mode="r", encoding="utf-8", newline="") as file:
                for row in file:
                    print(row)
            print("-------------" * 10)            
        except FileNotFoundError:
            print(f"nie można znaleźć pliku: {self.opis}")
            return "Brak opisu"

#klasa Gry            
class Gra:
    def __init__(self):
        self.ekwipunek = ["portfel","klucze","telefon"]
        self.koszyk = []
        self.lista_zakupow = ["jajka", "ser", "ryż", "sól", "wino", "chleb", "papier toaletowy"]
    
    @staticmethod
    def pomoc():
        print("-------------" * 10)
        print("weź - weź przedmiot.")
        print("użyj - użyj przedmiotu z ekwipunku.")
        print("opisz - opis pomieszczenia.")
        print("sprawdź liste - sprawdza liste zakupów")
        print("sprawdź koszyk - sprawdź co masz w koszyku.")
        print("idź dalej - przechodzisz do kolejnej alejki.")
        print("wyjdź - koniec gry.")
        print("-------------" * 10)
    
    def sprawdz_liste_zakupow(self):
        print(f"Lista zakupów: {', '.join(self.lista_zakupow)}")


    def wez(self, mozliwosci):
        while True:
            co = input("Co chcesz zabrać? (Wpisz 'cofnij' aby zakończyć): ").lower()
            if co == "cofnij":  
                break
            elif co in mozliwosci:
                self.koszyk.append(co)
                print(f"Dodano {co} do koszyka.")
            else:
                print("Nie możesz tego zabrać.")

    def uzyj(self):
        print(f"Twój ekwipunek: {self.ekwipunek}")
        podaj_przedmiot = input("Czego chcesz użyć?:  ").lower()
        
        if podaj_przedmiot in self.ekwipunek:
            return podaj_przedmiot
        else:
            print("Nie masz takiego przedmiotu!")

    def sprawdz_koszyk(self):
        print(f"W koszyku: {', '.join(self.koszyk)}")
    
    @staticmethod
    def wyjdz():
        print("")
        print("-------------" * 10)
        print("SZKODA ŻE KOŃCZYSZ")
        print("-------------" * 10)
        exit()

